class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler

  before_action :logged_in_user

  private

  def current_ability
    @current_ability ||= Ability.new(@user)
  end

  def encode_token(payload)
   #JWT.encode(payload, 'yourSecret')
   JWT.encode(payload,  Rails.application.secrets[:jwt_secret_key_base])
  end

  def auth_header
   # { Authorization: 'Bearer <token>' }
   request.headers['Authorization']
  end

  def decoded_token
   if auth_header
     token = auth_header.split(' ')[1]
     # header: { 'Authorization': 'Bearer <token>' }
     begin
      # JWT.decode(token, 'yourSecret', true, algorithm: 'HS256')
       JWT.decode(token, Rails.application.secrets[:jwt_secret_key_base], true, algorithm: 'HS256')
     rescue JWT::DecodeError
       nil
     end
   end
  end

  def logged_in_user
   if decoded_token
     user_id = decoded_token[0]['user_id']
     @user = User.find_by(id: user_id)
   end
  end

  def logged_in?
   !!logged_in_user
  end

  def authorized
   render json: { message: 'Please log in' }, status: :unauthorized unless logged_in?
  end
end
