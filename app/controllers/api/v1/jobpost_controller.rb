module Api
  module V1
      class JobpostController < ApplicationController
        load_and_authorize_resource
            before_action :jobpost_params , only:[:create, :update]
            before_action :authorized, only: [:create, :destroy,:update]
            # GET /jobpost
            def index
              json_response(@jobposts)
            end

            # POST /todos
            def create

              @jobpost = Jobpost.new(jobpost_params)
              @jobpost.user = @user
              @jobpost.save
              json_response(@jobpost, :created)
            end

            # GET /jobpost/:id
            def show
              json_response(@jobpost)
            end

            # PUT /jobpost/:id
            def update
              @jobpost.update(jobpost_params)
              json_response(@jobpost)
            end

            # DELETE /jobpost/:id
            def destroy
              @jobpost.destroy
              head :no_content
            end

            private

            def jobpost_params
              params.permit(:title, :description)
            end

          end
      end
    end
