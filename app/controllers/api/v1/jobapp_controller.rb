module Api
  module V1
      class JobappController < ApplicationController
            load_and_authorize_resource
          #  before_action :set_jobapp, only: [:show, :update, :destroy]
            before_action :jobapp_params , only:[:create]
            before_action :authorized, only: [:create, :destroy,:update,:show]
            before_action :set_jobpost, only: [:create]

            # GET /jobapp
            def index
              @jobapp = Jobapp.all
              json_response(@jobapp)
            end

            # POST /todos
            def create

                  @jobapp = Jobapp.new()
                  @jobapp.user =@user
                  @jobapp.jobpost =@jobpost
                  @jobapp.save
                  json_response(@jobapp, :created)

            end

            # GET /jobapp/:id
            def show
              if @user.is_admin?
                @jobapp.seen = true
                @jobapp.save
                json_response(@jobapp)
              else
                json_response(@jobapp)
              end
            end
            # DELETE /jobapp/:id
            def destroy
              if @user
              @jobapp.destroy
              head :no_content
              end
            end

            private

            def jobapp_params
              # whitelist params
              params.permit(:jobpost)
            end

            # def set_jobapp
            #   @jobapp = Jobapp.find(params[:id])
            # end
            def set_jobpost
              @jobpost = Jobpost.find(params[:jobpost_id])
            end



          end
      end
  end
