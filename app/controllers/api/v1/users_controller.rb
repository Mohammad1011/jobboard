module Api
  module V1
    class UsersController < ApplicationController
      before_action :authorized, only: [:auto_login, :index]


        def index
          @session[:user_id] = @user.id
           render json: [email: @user.email,id: @user.id]
        end
        # REGISTER
        def create
          @user = User.create(user_params)
          if @user.valid?

            token = encode_token({user_id: @user.id})
            render json: {user: @user, token: token}
          else
            render json: {error: "Invalid email or password"}
          end
        end

        # LOGGING IN
        def login
          @user = User.find_by(email: params[:email])
          if @user && @user.authenticate(params[:password])
            token = encode_token({user_id: @user.id})
            render json: {user: @user.email, token: token}
          else
            render json: {error: "Invalid email or password"}
          end
        end



        def auto_login

          render json: [email: @user.email,id: @user.id]
        end

        def destroy
          @user = nil
          render json: {status: "logged_out"}
        end

        private

        def user_params
          params.permit(:email, :password)
        end
      end
  end
end
