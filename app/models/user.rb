class User < ApplicationRecord
  has_many :jobposts
  has_many :jobapps
  has_secure_password
end
