Rails.application.routes.draw do


    namespace :api do
      namespace :v1 do
        resources :jobpost
        resources :jobapp
        resources :users do
          collection do
            post :login
            get :auto_login
          end
        end
        #resources :session
      end
    end


end
