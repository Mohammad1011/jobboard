class CreateJobapps < ActiveRecord::Migration[6.1]
  def change
    create_table :jobapps do |t|

      t.boolean :seen , default: false

      t.timestamps
    end
  end
end
