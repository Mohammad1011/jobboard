class AddForeignkeys < ActiveRecord::Migration[6.1]
  def change


    add_reference :jobposts, :user
    add_foreign_key :jobposts, :users

    add_reference :jobapps, :user
    add_foreign_key :jobapps, :users
    
    add_reference :jobapps, :jobpost
    add_foreign_key :jobapps, :jobposts
  end
end
